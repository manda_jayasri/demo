package com.main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.dao.EmployeeDao;
import com.exception.DepartmentException;
import com.exception.EmployeeException;
import com.model.Account;
import com.model.Department;
import com.model.Employee;
import com.service.DepartmentService;
import com.service.DepartmentServiceImpl;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

public class MainApp {

	public static void main(String[] args) {
		System.out.println("Adding Departments");
		System.out.println("-----------------------------------");
		Department department1 = new Department(1, "HR");
		Department department2 = new Department(2, "Training");
		Department department3 = new Department(3, "Developer");
		Department department4 = new Department(4, "Testing");
		Department department5 = new Department(5, "Sales");
		Set<Department> departments = new HashSet<Department>();
		departments.add(department1);
		departments.add(department2);
		departments.add(department3);
		departments.add(department4);
		departments.add(department5);
		DepartmentService departmentService = new DepartmentServiceImpl();
		List<Department> dbDepartments = null;
		try {
			for (Department department : departments) {
				departmentService.addDepartment(department);
			}
			System.out.println("Fecthing Departments...");
			dbDepartments = departmentService.getAllDepartments();
			System.out.println("Total Departments: " + dbDepartments.size());
			for (Department department : dbDepartments) {
				System.out.println("-----------------------------------");
				System.out.println(" Id: " + department.getDeptId());
				System.out.println(" Name: " + department.getDeptName());
			}

		} catch (DepartmentException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("Adding Employee with Account");
		System.out.println("-----------------------------------");

		Account account1 = new Account(1, "AC1234");
		Employee employee1 = new Employee(1, "Anusha", "anu@hcl.com", 1500.00);
		account1.setEmployee(employee1);
		employee1.setAccount(account1);

		Account account2 = new Account(2, "AC12324");
		Employee employee2 = new Employee(2, "Bhoomi", "bhommi@hcl.com", 3500.00);
		account2.setEmployee(employee2);
		employee2.setAccount(account2);

		Account account3 = new Account(3, "AC2222");
		Employee employee3 = new Employee(3, "Soori", "soori@hcl.com", 6500.00);
		account3.setEmployee(employee3);
		employee3.setAccount(account3);

		Account account4 = new Account(4, "AC3333");
		Employee employee4 = new Employee(5, "Rama", "rama@hcl.com", 7500.00);
		account4.setEmployee(employee4);
		employee4.setAccount(account4);

		Account account5 = new Account(5, "AC4444");
		Employee employee5 = new Employee(6, "Raja", "raja@hcl.com", 2500.00);
		account5.setEmployee(employee5);
		employee5.setAccount(account5);

		Account account6 = new Account(6, "AC5555");
		Employee employee6 = new Employee(7, "Rani", "rani@hcl.com", 4840.00);
		account6.setEmployee(employee6);
		employee6.setAccount(account6);

		Account account7 = new Account(5, "AC6666");
		Employee employee7 = new Employee(6, "Siri", "siri@hcl.com", 2500.00);
		account7.setEmployee(employee7);
		employee7.setAccount(account7);

		Employee employee8 = new Employee(7, "Divya", "divya@hcl.com", 4840.0);

		List<Employee> employees = new ArrayList<Employee>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		employees.add(employee4);
		employees.add(employee5);
		employees.add(employee6);
		employees.add(employee7);
		employees.add(employee8);
		EmployeeService employeeService = new EmployeeServiceImpl();
		List<Employee> dbEmployees = null;
		try {
			for (Employee employee : employees) {
				employeeService.addEmployee(employee);
			}
			System.out.println("Fecthing Employees...");
			dbEmployees = employeeService.getAllEmployees();
			for (Employee employee : dbEmployees) {
				System.out.println("-----------------------------------");
				System.out.println(" Id		: " + employee.getEmpId());
				System.out.println(" Name	: " + employee.getEmpName());
				System.out.println(" Email  : " + employee.getEmail());
				System.out.println(" Account Dt: ");
				if (employee.getAccount() != null) {
					System.out.println("  ->Id     : " + employee.getAccount().getAccountId());
					System.out.println("  ->A/C No.: " + employee.getAccount().getAccountNumber());
				}
			}
		} catch (EmployeeException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("-----------------------------------");
		System.out.println("Assign Department to Employees");
		try {
			System.out.println("Employees...");
			for (Employee employee : dbEmployees) {

				switch (employee.getEmpName()) {
				case "Anusha":
					employee.setDepartment(dbDepartments.get(1));
					break;

				case "Siri":
					employee.setDepartment(dbDepartments.get(2));
					break;

				case "Divya":
					employee.setDepartment(dbDepartments.get(3));
					break;
				case "Raja":
					employee.setDepartment(dbDepartments.get(4));
					break;
				case "Rani":
					employee.setDepartment(dbDepartments.get(0));
					break;

				case "Soori":
					employee.setDepartment(dbDepartments.get(0));
					break;

				case "Bhoomi":
					employee.setDepartment(dbDepartments.get(0));
					break;

				default:
					break;
				}
				employeeService.updateEmployeeById(employee, employee.getEmpId());

			}
		} catch (EmployeeException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("-----------------------------------");
		System.out.println("Fecthing Department data with Collection of Employee..");

		try {
			List<Department> dbDepartments1 = departmentService.getAllDepartments();
			for (Department department : dbDepartments1) {
				System.out.println("-----------------------------------");
				System.out.println("-Department Data------");
				System.out.println(" Id: " + department.getDeptId());
				System.out.println(" Name: " + department.getDeptName());
				if (department.getEmployees().size() > 0) {
					for (Employee employee : department.getEmployees()) {
						System.out.println("@Employees Data");
						System.out.println(" Id   : " + employee.getEmpId());
						System.out.println(" Name : " + employee.getEmpName());
						System.out.println(" Email: " + employee.getEmail());
						if (employee.getAccount() != null) {
							System.out.println(" Account Dt: ");
							System.out.println(" -->Id	   : " + employee.getAccount().getAccountId());
							System.out.println(" -->A/C No.: " + employee.getAccount().getAccountNumber());
						}
						if (employee.getDepartment() != null) {
							System.out.println(" Department Dt: ");
							System.out.println(" -->Id		  : " + employee.getDepartment().getDeptId());
							System.out.println(" -->Dept Name : " + employee.getDepartment().getDeptName());
						}
						System.out.println();
					}
				}
			}

		} catch (DepartmentException e) {
			System.err.println(e.getMessage());
		}

	}

}
