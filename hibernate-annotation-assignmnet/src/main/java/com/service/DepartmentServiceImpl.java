package com.service;

import java.util.ArrayList;
import java.util.List;

import com.dao.DepartmentDao;
import com.dao.DepartmentDaoImpl;
import com.exception.DepartmentException;
import com.model.Department;

public class DepartmentServiceImpl implements DepartmentService {

	public Department addDepartment(Department department) throws DepartmentException {
		DepartmentDao dao = new DepartmentDaoImpl();
		if (department != null) {
			dao.addDepartment(department);
		} else {
			throw new DepartmentException("Department data is null");
		}
		return department;
	}

	public Department readDepartmentById(int departmentId) throws DepartmentException {
		DepartmentDao dao = new DepartmentDaoImpl();
		Department dbDepartment = null;
		if (departmentId > 0) {
			dbDepartment = dao.readDepartmentById(departmentId);
		} else {
			throw new DepartmentException("Enter valid department Id");
		}
		if (dbDepartment != null) {
			return dbDepartment;
		} else {
			throw new DepartmentException("No data found regarding given department id: " + departmentId);
		}
	}

	public Department updateDepartmentById(Department department, int departmentId) throws DepartmentException {
		DepartmentDao dao = new DepartmentDaoImpl();
		Department dbDepartment = null;
		if (department != null) {
			dbDepartment = dao.updateDepartmentById(dbDepartment, departmentId);
		} else {
			throw new DepartmentException("Department data is null");
		}

		return dbDepartment;
	}

	public boolean deleteDepartmentById(int departmentid) throws DepartmentException {
		boolean result = false;
		DepartmentDao dao = new DepartmentDaoImpl();
		try {
			if (departmentid > 0) {
				result = dao.deleteDepartmentById(departmentid);
			} else {
				throw new DepartmentException("Enter valid department Id only..");
			}
		} catch (Exception e) {
			throw new DepartmentException("No Department Data Found regarding given id:" + departmentid);
		}
		return result;
	}

	public List<Department> getAllDepartments() throws DepartmentException {
		DepartmentDao dao = new DepartmentDaoImpl();
		List<Department> dbDepartments = new ArrayList<Department>();
		dbDepartments = dao.getAllDepartments();
		if (dbDepartments.size() > 0) {
			return dbDepartments;
		} else {
			throw new DepartmentException("Enter valid department Id only..");
		}
	}

}
