package com.service;

import java.util.ArrayList;
import java.util.List;

import com.dao.EmployeeDao;
import com.dao.EmployeeDaoImpl;
import com.exception.EmployeeException;
import com.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {

	public Employee addEmployee(Employee employee) throws EmployeeException {
		EmployeeDao dao = new EmployeeDaoImpl();
		if (employee != null) {
			dao.addEmployee(employee);
		} else {
			throw new EmployeeException("Employee data is null");
		}
		return employee;
	}

	public Employee readEmployeeById(int employeeId) throws EmployeeException {
		EmployeeDao dao = new EmployeeDaoImpl();
		Employee dbEmployee = null;
		if (employeeId > 0) {
			dbEmployee = dao.readEmployeeById(employeeId);
		} else {
			throw new EmployeeException("Enter valid employee Id");
		}
		if (dbEmployee != null) {
			return dbEmployee;
		} else {
			throw new EmployeeException("No data found regarding given employee id: " + employeeId);
		}
	}

	public Employee updateEmployeeById(Employee employee, int employeeId) throws EmployeeException {
		EmployeeDao dao = new EmployeeDaoImpl();
		Employee dbEmployee = null;
		if (employee != null) {
			dbEmployee = dao.updateEmployeeById(employee, employeeId);
		} else {
			throw new EmployeeException("Employee data is null");
		}

		return dbEmployee;
	}

	public boolean deleteEmployeeById(int employeeid) throws EmployeeException {
		boolean result = false;
		EmployeeDao dao = new EmployeeDaoImpl();
		try {
			if (employeeid > 0) {
				result = dao.deleteEmployeeById(employeeid);
			} else {
				throw new EmployeeException("Enter valid employee Id only..");
			}
		} catch (Exception e) {
			throw new EmployeeException("No Employee Data Found regarding given id:" + employeeid);
		}
		return result;
	}

	public List<Employee> getAllEmployees() throws EmployeeException {
		EmployeeDao dao = new EmployeeDaoImpl();
		List<Employee> dbEmployees = new ArrayList<Employee>();
		dbEmployees = dao.getAllEmployees();
		if (dbEmployees.size() > 0) {
			return dbEmployees;
		} else {
			throw new EmployeeException("Enter valid employee Id only..");
		}
	}

}
