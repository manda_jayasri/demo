package com.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.exception.DepartmentException;
import com.model.Department;

public class DepartmentDaoImpl implements DepartmentDao {

	public Department addDepartment(Department department) throws DepartmentException {
		Session session = HibernateConnection.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			String sql = "SELECT * FROM Department WHERE deptName = :name";

			Query q = session.createNativeQuery(sql);
			q.setParameter("name", department.getDeptName());

			List<Department> dbDepartments = q.list();

			if (dbDepartments.size() < 0
					&& dbDepartments.get(0).getDeptName().equalsIgnoreCase(department.getDeptName())) {
				System.err.println("Already Exist");
			} else {
				session.save(department);
				transaction.commit();
				System.out.println("Department added successfully!");

			}

		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return department;
	}

	public Department readDepartmentById(int departmentId) throws DepartmentException {
		Session session = HibernateConnection.getSession();
		Department dbDepartment = null;
		try {
			dbDepartment = session.get(Department.class, departmentId);
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return dbDepartment;
	}

	public boolean deleteDepartmentById(int departmentid) throws DepartmentException {
		Session session = HibernateConnection.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			Department dbDepartment = session.get(Department.class, departmentid);
			if (dbDepartment != null) {
				session.delete(dbDepartment);
				System.out.println("Department deleted successfully!");
				transaction.commit();
			} else {
				throw new DepartmentException("Data not found!");
			}

		} catch (HibernateException e) {
			e.printStackTrace();
		}

		return true;
	}

	public Department updateDepartmentById(Department department, int departmentId) throws DepartmentException {
		Session session = HibernateConnection.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.update(department);
			transaction.commit();
			System.out.println("Department updated successfully!");
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return department;
	}

	public List<Department> getAllDepartments() throws DepartmentException {
		Session session = HibernateConnection.getSession();
		List<Department> dbDepartments = new ArrayList<Department>();

		try {
			dbDepartments = session.createQuery("FROM Department").list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return dbDepartments;

	}

}
