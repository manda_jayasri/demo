package com.dao;

import java.util.List;

import com.exception.EmployeeException;
import com.model.Employee;

public interface EmployeeDao {

	public abstract Employee addEmployee(Employee employee) throws EmployeeException;

	public abstract Employee updateEmployeeById(Employee employee, int employeeId) throws EmployeeException;

	public abstract boolean deleteEmployeeById(int employeeid) throws EmployeeException;

	public abstract Employee readEmployeeById(int employeeId) throws EmployeeException;

	public abstract List<Employee> getAllEmployees() throws EmployeeException;
	
}
