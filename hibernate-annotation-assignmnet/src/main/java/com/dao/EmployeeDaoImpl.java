package com.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.exception.EmployeeException;
import com.model.Employee;

public class EmployeeDaoImpl implements EmployeeDao {

	public Employee addEmployee(Employee employee) throws EmployeeException {
		Session session = HibernateConnection.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			if (employee.getAccount() != null) {
				session.save(employee.getAccount());
			}
			session.save(employee);
			transaction.commit();
			System.out.println("Employee added successfully!");
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return employee;
	}

	public Employee readEmployeeById(int employeeId) throws EmployeeException {
		Session session = HibernateConnection.getSession();
		Employee dbEmployee = null;
		try {
			dbEmployee = session.get(Employee.class, employeeId);
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return dbEmployee;
	}

	public boolean deleteEmployeeById(int employeeid) throws EmployeeException {
		Session session = HibernateConnection.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			Employee dbEmployee = session.get(Employee.class, employeeid);
			if (dbEmployee != null) {
				session.delete(dbEmployee);
				System.out.println("Employee deleted successfully!");
				transaction.commit();
			} else {
				throw new EmployeeException("Data not found!");
			}

		} catch (HibernateException e) {
			e.printStackTrace();
		}

		return true;
	}

	public Employee updateEmployeeById(Employee employee, int employeeId) throws EmployeeException {
		Session session = HibernateConnection.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.update(employee);
			transaction.commit();
			System.out.println("Employee updated successfully!");
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return employee;
	}

	public List<Employee> getAllEmployees() throws EmployeeException {
		Session session = HibernateConnection.getSession();
		List<Employee> dbEmployees = new ArrayList<Employee>();

		try {
			dbEmployees = session.createQuery("FROM Employee").list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return dbEmployees;

	}
}
