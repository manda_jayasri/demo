package com.dao;

import java.util.List;

import com.exception.DepartmentException;
import com.model.Department;

public interface DepartmentDao {
	public abstract Department addDepartment(Department department) throws DepartmentException;

	public abstract Department updateDepartmentById(Department department, int departmentId) throws DepartmentException;

	public abstract boolean deleteDepartmentById(int departmentid) throws DepartmentException;

	public abstract Department readDepartmentById(int departmentId) throws DepartmentException;

	public abstract List<Department> getAllDepartments() throws DepartmentException;
}
	