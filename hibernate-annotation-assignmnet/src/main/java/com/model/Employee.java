package com.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int empId;

	@Column(length = 25)
	private String empName;

	@Column(name = "emp_email", length = 25)
	private String email;

	@Column(name = "emp_salary")
	private double salary;

	@OneToOne(cascade = CascadeType.ALL)
	private Account account;

	@ManyToOne(fetch = FetchType.LAZY)
	private Department department;

	public Employee() {
		super();

	}

	public Employee(int empId, String empName, String email, double salary) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.email = email;
		this.salary = salary;
	}

	public Employee(int empId, String empName, String email, double salary, Department department) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.email = email;
		this.salary = salary;

		this.department = department;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", email=" + email + ", salary=" + salary
				+ ", account=" + account + ", department=" + department + "]";
	}

}
