package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int accountId;

	@Column(name = "account_number", length = 10)
	private String accountNumber;

	@OneToOne(mappedBy = "account")
	private Employee employee;

	public Account() {
		super();
	}

	public Account(int accountId, String accountNumber) {
		super();
		this.accountId = accountId;
		this.accountNumber = accountNumber;
	}

	public Account(int accountId, String accountNumber, Employee employee) {
		super();
		this.accountId = accountId;
		this.accountNumber = accountNumber;
		this.employee = employee;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "Account [accountId=" + accountId + ", accountNumber=" + accountNumber + ", employee=" + employee + "]";
	}

}
