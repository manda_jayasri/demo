package com.exception;

public class DepartmentException extends Exception {

	private String message;

	public DepartmentException() {
		super();
	}

	public DepartmentException(String message) {
		super();
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
